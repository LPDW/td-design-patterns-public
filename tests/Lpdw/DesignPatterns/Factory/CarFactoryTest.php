<?php

namespace Lpdw\DesignPatterns\Factory;

use PHPUnit\Framework\TestCase;

class CarFactoryTest extends TestCase
{
    private static $factory;

    /**
     * @before
     */
    public function init()
    {
        self::$factory = new CarFactory();
    }

    /**
     * @test
     */
    public function shouldCreateCar()
    {
        $car = self::$factory->create();
        $this->assertEquals(4, $car->countWheels());
    }
}

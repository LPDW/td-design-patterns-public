<?php

namespace Lpdw\DesignPatterns\Proxy;

use PHPUnit\Framework\TestCase;

class FileProxyTest extends TestCase
{
    /**
     * @test
     */
    public function shouldGetSizeWithoutLoadContent()
    {
        self::assertFileExists(__DIR__ . '/../../../resources/huge.file.txt');
        $file = new ProxyFile(__DIR__ . '/../../../resources/huge.file.txt');
        self::assertEquals($file->getSize(), 18);
    }

    /**
     * @test
     */
    public function shouldGetContent()
    {
        self::assertFileExists(__DIR__ . '/../../../resources/huge.file.txt');
        $file = new ProxyFile(__DIR__ . '/../../../resources/huge.file.txt');
        self::assertEquals($file->getContent(), 'Huge file content.');
    }
}
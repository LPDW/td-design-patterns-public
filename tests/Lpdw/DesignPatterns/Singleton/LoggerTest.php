<?php

namespace Lpdw\DesignPatterns\Singleton;

use PHPUnit\Framework\TestCase;

class LoggerTest extends TestCase
{
    /**
     * @test
     */
    public function shouldGetInstance()
    {
        $logger = Logger::getInstance();
        $this->assertInstanceOf(Logger::class, $logger);
    }

    /**
     * @test
     */
    public function shouldAlwaysReturnTheSameInstance()
    {
        $logger = Logger::getInstance();
        $this->assertInstanceOf(Logger::class, $logger);

        self::assertTrue(Logger::getInstance() === $logger);
    }

    /**
     * @test
     */
    public function shouldNotWorkWithNew()
    {
        try {
            $logger = new Logger();
            $this->assertInstanceOf(Logger::class, $logger);
        } catch (\Throwable $e) {
            self::assertEquals('Call to private Lpdw\DesignPatterns\Singleton\Logger::__construct() from context \'Lpdw\DesignPatterns\Singleton\LoggerTest\'', $e->getMessage());
        }
    }
}

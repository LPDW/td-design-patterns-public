<?php

namespace Lpdw\DesignPatterns\Adapter;

use PHPUnit\Framework\TestCase;

class DiscoursAdapterTest extends TestCase
{
    /**
     * @test
     */
    public function shouldAdapteDiscours()
    {
        $discours = new PhpDiscours("Présentation de PHP");
        $discoursAdapter = new DiscoursAdapter($discours);
        $speakerApplication = new SpeakerApplication();

        $this->assertEquals("Présentation de PHP", $speakerApplication->talk($discoursAdapter));
    }
}

<?php

namespace Lpdw\DesignPatterns\Factory;

class CarFactory extends Factory
{
    public function create():Vehicle
    {
        return new Car();
    }
}

<?php

namespace Lpdw\DesignPatterns\Factory;

class Car implements Vehicle
{
    private $color;

    public function countWheels():int
    {
        return 4;
    }
}

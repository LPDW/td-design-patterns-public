<?php

namespace Lpdw\DesignPatterns\Proxy;


class RealFile implements SubjectFile
{

    private $filePath;
    private $content;

    public function __construct($filePath)
    {
        $this->filePath = $filePath;
        $this->content = file_get_contents($this->filePath);
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getSize(): int
    {
        return filesize($this->filePath);
    }
}

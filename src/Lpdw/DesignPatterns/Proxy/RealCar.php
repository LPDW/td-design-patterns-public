<?php

namespace Lpdw\DesignPatterns\Proxy;


class RealCar implements SubjectCar
{

    public function driveCar()
    {
        return 'Car has been driven!';
    }
}
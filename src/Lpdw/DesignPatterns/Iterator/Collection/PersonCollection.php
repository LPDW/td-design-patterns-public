<?php

namespace Lpdw\DesignPatterns\Iterator\Collection;

use Lpdw\DesignPatterns\Iterator\Model\Person;

class PersonCollection implements \Iterator
{
    private $currentIndex;

    private $collection;

    public function __construct(array $collection)
    {
        //TODO
    }

    public function current():?Person
    {
        //TODO
    }

    public function next()
    {
        //TODO
    }

    public function key():int
    {
        //TODO
    }

    public function valid():bool
    {
        //TODO
    }

    public function rewind()
    {
        //TODO
    }
}

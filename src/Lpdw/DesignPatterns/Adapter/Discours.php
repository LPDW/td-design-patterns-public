<?php

namespace Lpdw\DesignPatterns\Adapter;

interface Discours
{
    public function lireTexte():string;
}
